---
title: "Applesauce Muffins"

category: "quick breads"
origin: "Mary Arthur"
serves: "4 dozen"
---
- 1 c margarine, softened
- 2 c sugar
- 2 eggs
- 3 tsp vanilla
- 4 c flour, sifted
- 1 tsp cloves
- 2 tsp allspice
- 3 tsp cinnamon
- 2 tsp soda
- 1 (1 lb) can applesauce (2 c)

Cream margarine, sugar, eggs and 1 tsp vanilla. Sift together flour and spices; add to the creamed mixture. Blend 2 tsp vanilla into the applesauce, add soda, then blend with the batter using a wooden spoon. Batter will be stiff, so stir until well mixed. Spoon into greased muffin tines ½ full. Bake at 400 for 12 min. 4 dozen.

## Topping
- ½ c butter
- ½ c butter flavored Crisco
- 1 c brown sugar
- 1 c white sugar
