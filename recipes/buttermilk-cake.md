---
title: "Buttermilk Cake"

category: "cakes"
origin: "Mary Arthur"
---

Sift:
– 2 c flour
- 2 c sugar
- 1 tsp soda
	
Melt:
– 2 sticks margarine
- 4 tbsp cocoa
- 1 c water

Boil. Pour wet over dry then add **½ c buttermilk**, **2 eggs**, **1 tsp vanilla** and stir.

## Frosting:
Combine **1 stick margarine**, **4 tbsp cocoa**, **6 tbsp buttermilk** and heat over low heat. Bring to a rapid boil and remove from heat. Add **3 ½ c powdered sugar** and **1 tsp vanilla**.

