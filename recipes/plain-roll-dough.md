---
title: "Plain Roll Dough"

category: "breakfast foods"
origin: "Deb Arthur"
serves: "18 cinnamon rolls"
---

- 2 pkgs active dry yeast
- ½ c water
- 1 c milk
- ¼ c shortening
- 3 tbsp sugar
- 1 tsp salt
- 1 well-beaten egg
- 3 ½ - 4 c sifted enriched flour

Mix water, milk, shortening and warm in saucepan. Shortening need not be melted. In bowl, beat egg and add liquid ingredients. Mix dry yeast with 1 ½ c flour, sugar and salt. Beat with electric mixer for 2 min. with liquid ingredients. Slowly add remaining flour and beat with spoon to form a soft dough. Let rise10 min. (or refrigerate). Shape into desired rolls. Bake in greased pans at 370 for 25 min.
