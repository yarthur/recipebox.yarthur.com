---
title: "Ginger Snaps"

category: "cookies & bars"
origin: "Mary Arthur"
---
- 3 c sifted all purpose flour
- ½ c sugar
- 1 tsp salt
- 1 tsp baking soda
- 2 tsp ginger
- 2 tsp cinnamon
- ¾ c shortening
- 1 1/3 c molasses
- 1 tbsp vinegar

Sift flour, sugar, salt, soda and spices into a mixing bowl. Cut in shortening with a pastry blender to resemble course meal. Heat molasses to boiling point. Add vinegar and gradually stir into flour-shortening mixture. Drop dough, ½ tsp at a time, onto greased baking sheets. Flatten to 1/16” thick by stamping with glass covered with a damp cloth. Bake in 400 oven 6-7 min or until edges have lightly browned.
