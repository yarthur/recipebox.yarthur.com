---
title: "Jello Ribbon Salad"

category: "sides"
origin: "Deb Arthur"
---

- 1 3 oz box each of the following flavors of Jello:
  - Black Cherry
  - Cherry
  - Lime
  - Lemon
  - Orange
  - Peach
  - Strawberry
- 1 can sweetened condensed milk (Eagle Brand written on the original recipe)
- Cool Whip
- Salad Dressing/Miracle Whip

Mix each layer in a bowl and pour into 9 X 13 in. pan. Allow at least 30 minutes between layers to set.

**Layers 1, 3, 5 & 7:** Dissolve Jello in 3⁄4 cup boiling water, add 3⁄4 cup cold water.

**Layers 2, 4 & 6:** Dissolve Jello in 3⁄4 cup boiling water, add 1⁄4 cup cold water and 1/3 can of sweetened condensed milk (Eagle Brand written on the original recipe.)

**Topping:** Mix 2 cups of Cool Whip with 1 Tbsp salad dressing (Miracle Whip). Keep stirring as it curdles at first. Spread evenly over the top when set.

