---
title: "Forralt Bor"

category: beverages
origin: "Mary Arthur"
tags:
    - hungarian
---
- 1 L red wine
- 1 L white wine
- 1 L water

mix

- 1 spoon cinnamon (not ground)
- egisz (2 pieces)
- szegfubors egisz tbsp (less)
- szegfuseg egisz same
- 5-6 tbsp sugar
	- or 3 tbsp honey
		
let sit 1 hr or more

- 1 lemon – grate rind in and squeeze lemon in

heat up QUICKLY

boiling – turn off

taste (maybe) 1 more sugar

