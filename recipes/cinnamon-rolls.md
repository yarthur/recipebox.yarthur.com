---
title: "Cinnamon Rolls"

category: "breakfast foods"
origin: "Deb Arthur"
---

Prepare [Plain roll dough](/recipes/plain-roll-dough) – roll out to an oblong or rectangle about ¼” thick.

Mix: 1 c brown sugar, 1 c granulated sugar, 1 tsp cinnamon, 1 tsp vanilla and about 2 tbsp milk. Spread a little on bottom of greased pan. Spread remaining mixture on rolled out dough. Add nuts or raisins as desired. Roll and seal edges. Cut into 1” slices. Let rise till double. Bake at 375 for 25 min.

