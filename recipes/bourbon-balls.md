---
title: "Bourbon Balls"

category: "cookies & bars"
origin: "Mary Arthur"
---

- 2 ½ c vanilla wafers (crushed)
- 1 c powdered sugar
- 2 tbsp cocoa
- 1 c nuts

Mix above together. Mix 3 tbsp white syrup with ¼ c bourbon and mix into dry ingredients. Press into balls and roll in powdered sugar.

