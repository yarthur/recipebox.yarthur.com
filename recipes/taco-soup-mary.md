---
title: "Taco Soup (Mary Arthur's version)"

category: soups
origin: "Mary Arthur"
serves: "6-8"
---

- 1 ½ lbs ground beef
- ½ c chopped onion
- 1 can (28 oz) whole tomatoes with juice
- 1 can (14 oz) kidney beans with juice
- 1 can (17 oz) corn with juice
- 1 can (8 oz) tomato sauce
- 1 pkg taco seasoning
- 1-2 c water
- salt, pepper to taste
- 1 c grated cheddar cheese

Brown beef in large heavy kettle, drain and add onions. Cook until onions are tender. Add remaining ingredients except cheese. Simmer for 15 min.
