---
title: Sourdough

category: "yeast breads"
origin: "Tim Arthur"
---

_Feeding_: Every 4-8 days. Don’t use the same day and don’t let it get down to less than one cup. It can go longer between feedings, but don’t push it. To feed, add **1 c milk**, **1 c flour** and **¼-½ c sugar**.
