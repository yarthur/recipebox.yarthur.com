---
title: "Toll House Chocolate Chip Cookies"

category: "cookies & bars"
origin: "Mary Arthur"
---
- 1 c margarine
- ¾ c sugar
- ¾ c brown sugar
- 2 eggs
- 1 tsp vanilla
- 2 ½ c flour
- 1 tsp salt
- 1 tsp baking soda
- 1 pkg chips

350 8-10 min
