---
title: "Sourdough Bread"

category: "yeast breads"
origin: "Tim Arthur"
---
Mix together:
- 1 pkg active dry yeast
- 2 tsp salt
- 2 tsp sugar
- 2 ½ c flour
- 1 c sourdough
- 1 ½ c warm water

Beat ¾ minute, cover, let rise until bubbly, about 1 ½ hours.

Then combine:
- 3 c flour
- ½ tsp soda

and stir into dough.

Add enough flour to make stiff dough – at least ½ cup. Knead 5-8 min or longer. Divide in half, cover, let rise 10 min. shape into round or pan loaves and place on greased sheet, slash tops 1-3 times. Let rise until double, then bake at 350 for 35-40 min. Brush with butter.

