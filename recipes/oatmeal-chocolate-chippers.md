---
title: "Oatmeal Chocolate Chippers"

category: "cookies & bars"
origin: "Mary Arthur"
---
- 1 ¼ c flour
- ½ c cocoa
- 2 tsp soda
- ½ tsp salt
- 1 c margarine
- 1 c brown sugar
- ½ c sugar
- 1 tsp vanilla
- 2 eggs
- 12 oz semi sweet chips
- 2 c oatmeal
- 1 c nuts
