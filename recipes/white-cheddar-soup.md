---
title: "White Cheddar Soup"

category: "soups"
origin: "Mary Arthur"
serves: 6
---
- 2 to 3 large head garlic
- ½ c butter, melted
- 2 c chopped leeks
- 6 c ready-to-serve chicken broth
- ½ tsp salt
- ¼ tsp coarsely ground pepper
- 2 c milk
- 1 ½ c (6 oz) shredded cheddar cheese, divided
- (smoked sausage or cooked ham)

Preheat oven to 400. Place garlic in a medium baking dish. Pour butter over garlic. Cover tightly with a double thickness of aluminum foil. Bake 35-40 min. until garlic is tender. Remove garlic with slotted spoon, reserving butter. In a large pan heat 3 tbsp reserving butter over medium-high until hot. Add leeks and cook, stirring constantly until softened. Add potatoes and cook two minutes more. Add chicken broth, salt and pepper. Bring to a boil. Reduce heat and simmer uncovered 25-30 min. or until potatoes are tender. Remove from heat and add roasted garlic. Place half of soup in blender or food processor container and blend until smooth. Repeat with remaining soup. Return soup to saucepan and place over low heat. Add milk. Heat, stirring occasionally, until soup is hot. Add 1 c cheese, ½ c at a time and heat 3-5 min. or until cheese is melted, stirring constantly. If desired, stir in shrimp, sausage, prosciutto or ham. Garnish each serving with chopped chives and remaining ½ c cheese. Serve immediately. Makes six servings of 1 ½ c each.
