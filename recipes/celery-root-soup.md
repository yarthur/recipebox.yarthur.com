---
title: "Celery Root Soup"

category: soups
tags:
    - vegan
    - vegetarian

origin: "Mary Arthur"
---

Peel and slice celery

Sautee in oil

Add a little salt

When it’s hot add ginger and a little soy sauce

(can add corn)

½ lemon squeezed

when soft, puree and water down (1/2 L +)

boil

(grate in a little lemon rind)

