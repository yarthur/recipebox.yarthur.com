---
title: "Portuguese Spice Muffins"

category: "quick breads"
origin: "Mary Arthur"
serves: 12 muffins
---

- 1 ½ c flour
- ½ c sugar
- ¼ tsp salt
- ½ tsp cinnamon
- ½ tsp allspice
- ¼ tsp nutmeg
- ½ tsp ginger
- 1 ½ tsp baking powder
- ½ tsp soda
- 1 egg, beaten
- ¼ c margarine, melted
- 1/3 c molasses
-  ¾ c buttermilk

Mix together flour, sugar, spices, baking powder and soda. In another bowl, whisk egg with melted margarine, molasses and buttermilk. Make a well in the dry ingredients and add liquid, stirring lightly. Spoon into greased muffin cups and bake until done. Bake at 400 for 15-20 minutes.
