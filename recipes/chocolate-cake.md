---
title: "Chocolate Cake"

category: "cakes"
origin: "Mary Arthur"
---
- 2 c sugar
- 1 c sour cream
- 2 eggs
- 3 tbsp cocoa
- 1 tsp soda
- pinch salt
- 2 ½ c flour
- 1 tsp vanilla

add **¾ c boiling water** put right in oven. Bake at 350 for 35 min or until done.
