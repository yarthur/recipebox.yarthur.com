---
title: "Sourdough Pancakes/Waffles"

category: "breakfast foods"
origin: "Tim Arthur"
---
- 1 c sourdough
- 1 c flour
- ¾ tsp soda
- ¼ tsp salt
- 1/3 c oil
- 1 egg
- enough milk for pouring (2/3 c)
