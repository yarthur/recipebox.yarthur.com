---
title: "Moist Cake Brownies"

category: "cookies & bars"
tags: 
    - chocolate
origin: "Deb Arthur"
---

- 2/3 C Butter
- 3/4 C Baking Cocoa
- 1/4 C Vegetable oil
- 2 C Sugar
- 4 Eggs
- 2 tsp vanilla
- 1-1/2 Flour
- 1 tsp Baking Powder
- 1 tsp Salt
- 2/3 C Semisweet Chocolate Chips
- 1/2 C Milk Chocolate Chips
- 1 C coarsly chopped Pecans (opt)

Melt butter in large bowl in microwave. Whisk in cocoa and oil until smooth. Stir in sugar. Add eggs one at a time, beating well after each addition. Stir in vanilla. Combine flour, baking powder and salt. Add to cocoa mixture. Stir in chocolate chips and pecans. Spread into a greased 13 x 9 x 2" pan. Bake at 350°F for 30 min. Line bottom of pan with waxed paper and grease pan and paper if you want to remove from pan after baking. Let set approx. 3 min. after removing from oven the invert on wax paper covered cooling rack. Then invert one more time onto wax paper covered rack so they are right side up. I like to use European Style Cocoa (Dutch Processed) in this recipe. It has a richer flavor.
