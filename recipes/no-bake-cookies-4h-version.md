---
title: "No Bake Cookies (4-H version)"

category: "cookies & bars"
origin: "Mary Arthur"
---
- 2 c sugar
- ½ c milk
- 1 stick butter
- 3-4 tbsp cocoa
- ½ c peanut butter
- 2 ½ - 3 c quick cooking oatmeal
- 2 tsp vanilla
- (½-1 c chopped nuts)

Boil sugar, milk, butter and cocoa for 1 ½ min. Start timing after mixture reaches a full rolling boil. Remove from heat, add peanut butter, stir. Add oatmeal, vanilla and nuts. Beat till blended. Drop by teaspoons or put in greased 12x15” pan, cut in bars.
