---
title: "Kolache"

category: "yeast breads"
origin: "Mary Arthur"
serves: "20 rolls"
---

- 1 pkg active dry yeast
- ¼ c warm water
- 2 tbsp butter
- 2 tbsp sugar
- 1 tsp salt
- ½ c milk, scalded
- 1 egg
- 2 ¼ - 2 ½ c flour

**Filling**:
- 1 c pitted cooked prunes or cooked dried apricots, chopped
- 3 tbsp sugar
- 2 tsp grated orange rind
- ¼ tsp cinnamon

Soften yeast in warm water. In mixing bowl combine butter, sugar, salt and milk. Cool to lukewarm. Stir in egg and yeast. Gradually add flour to form a stiff dough, beating well after each addition. Cover; let rise in warm place until doubled 30 min – 1 hr. Prepare filling by combining ingredients.

Roll our dough on well-floured surgace to a 15x12” rectangle. Cut into 20 3” squares. In center of each, place 1 tsp filling. Bring corners of square to center; seal to enclose fruit. Place 2” apart on greased cookie sheets, seam side up. Cover; let rise until doubled 30-40 min. Bake at 400 for 12-15 min. until golden brown. If desired rolls may be brushed with soft or melted butter and sprinkled with sugar.
