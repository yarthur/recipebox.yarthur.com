---
title: "Surprise Meringues"

category: "cookies & bars"
origin: "Mary Arthur"
---

- 2 egg whites
- 1/8 tsp salt
- 1/8 tsp cream of tartar
- 1 tsp vanilla
- ¾ c sugar
- 1 c chocolate chips
- (1/4 c chopped nuts)

Beat first four ingredients until soft peaks form. Add sugar gradually, beating until peaks are stiff. Fold in chips and nuts. Cover cookie sheet with brown paper. Drop by rounded tsp. Bake 300 for 25 min.
