---
title: "Almond Tea Bread"

category: "quick breads"
origin: "Mary Arthur"
---
Cream together:
- 2 ¼ c sugar
- 3 eggs
- 1 1/8 c vegetable oil

Sift together:
- 3 c flour
- 1 ½ tsp salt
- 1 ½ tsp baking powder
 
Add flour mixture to sugar mixture alternately with **1 c milk**. Add flavorings and beat 1-2 min.
- 1 ½ tbsp poppy seeds
- 1 ½ tbsp almond extract
- 1 ½ tbsp butter extract
- 1 ½ tbsp vanilla

Bake in 5 small loaf pans at 350 for 40-45 min.

##Glaze:
- ¾ c sugar
- ¼ c orange juice
- ½ tsp almond extract
- ½ tsp butter extract
- ½ tsp vanilla

Mix glaze ingredients – cook over low heat till sugar melts. Drip over each loaf while still in pan hot from oven. You may poke hole with a toothpick in the top to allow glaze to soak in. Remove from pans and cool.

