---
title: "Russian Tea Cakes"

category: "cookies & bars"
origin: "Mary Arthur"
---

- ½ c pecan halves, toasted
- 1 c powdered sugar, divided
- 1 ½ c all-purpose flour
- 1/3 c unsweetened cocoa
- dash salt
- 1 c butter, softened
- ½ tsp vanilla

Place pecans and ¼ c powdered sugar in food processor and process until nuts are small enough. Whisk together flour, cocoa, salt and nut mixture – set aside. Beat butter and remaining ¾ c powdered sugar until light and fluffy. Beat in vanilla. Gradually beat in flour mixture just until mixed. Refrigerate dough, covered, at least on e hour and preferably no longer than 3 hours. Measure dough by 1 ¼” scoop. Roll into 1” balls. Bake at 350 for 18-20 min or until set. Cool on baking sheets 2-3 min. Roll warm cookies in powdered sugar.
