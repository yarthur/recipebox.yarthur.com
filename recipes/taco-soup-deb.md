---
title: Taco Soup (Deb Arthur's version)

category: soups
origin: "Deb Arthur"
---

- 2 lbs hamburger
- 2 cans Hormel Chili (no beans)
- 1 onion
- Garlic
- 2 cans stewed tomatoes
- 2 cans Rotel tomatoes
- 1 lb Velveeta

Brown hamburger and onion; drain. Dump all ingredients except cheese in crockpot. Add Velveeta approximately 20 minutes before serving. Serve with sour cream and Fritos.
