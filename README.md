The Arthur Recipe Box _(recipebox.yarthur.com)_
===============================================

[![pipeline status](https://gitlab.com/yarthur/recipebox.yarthur.com/badges/master/pipeline.svg)](https://gitlab.com/yarthur/recipebox.yarthur.com/-/commits/master)

> A collection of family recipes for and by the Arthurs.

Background
----------
My family, much as many other families, have several beloved recipes that, over time, have not be so much handed down, as allowed to tumble down, blow around, and get lost in the shuffle. So to hopefully bring some sense to the chaos, and make for a single, easily-accessible place to house these recipes, I'm throwing together a quick and dirty website.

Install
-------
**Dependencies:** In order to run this locally, you'll want to have Git and Node installed on your system. Assuming that's the case, here's how you get things working...

To install the code needed and get things rolling, follow these simple steps:

1) Clone the repository locally.
	```sh
	git clone git@gitlab.com:yarthur/recipebox.yarthur.com.git
	```
2) Install Node dependencies.
	```sh
	npm i
	```
3) To build, you can run the build script. To run a local instance, you can also run the serve script.
	```sh
	# Build it once.
	npm run build

	# Creates a local server instance, watches for changes, and updates the build accordingly.
	npm run serve
	```
