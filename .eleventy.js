module.exports = function(eleventyConfig) {
  eleventyConfig.addCollection("categories", function(collection) {
    let all = collection.getAll(),
      categories = new Set();

    all.forEach(function(item) {
      if ("category" in item.data) {
        categories.add(item.data.category);
      }
    });

    return [...categories];
  });

  eleventyConfig.addCollection("recipes", function(collection) {
    return collection.getFilteredByGlob("recipes/*.md");
  });
};
